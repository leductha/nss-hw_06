# PP Destroyer 3000

## Autoři

- Jan Novák
- Eva Hrušková
- Adam Vlk

## Technologie

- React
- Node.js
- MongoDB
- Docker
- GraphQL

## Popis

PP Destroyer 3000 je moderní webová aplikace navržená pro efektivní správu úkolů a projektů. Díky reaktivnímu uživatelskému rozhraní a využití nejnovějších technologií poskytuje výkonné nástroje pro teamovou spolupráci a osobní produktivitu.

## Instalace

Pro instalaci aplikace klonujte repozitář a následujte příkazy:

```bash
git clone https://example.com/ppdestroyer3000.git
cd ppdestroyer3000
npm install
npm start
